# -*- coding: utf-8 -*-


from collections import Counter
from scipy.stats import stats

# 加载pandas库
import pandas as pd
# 加载numpy库
import numpy as np


one_dataSet_train_path = 'handled_data/one_dataSet_train_'
one_dataSet_test_path = 'handled_data/one_dataSet_test_'
two_dataSet_train_path = 'handled_data/two_dataSet_train_'
two_dataSet_test_path = 'handled_data/two_dataSet_test_'
three_dataSet_train_path = 'handled_data/three_dataSet_train_'

train_path = 'train_and_test/train.csv'
test_path = 'train_and_test/test.csv'

register = 'register.csv'
create = 'create.csv'
launch = 'launch.csv'
activity = 'activity.csv'


# 构建训练集与测试集与特征
# 获取所有id 查看对应id是否在测试集例面出现过
def get_train_label(trainpath, testpath):
    train_reg = pd.read_csv(trainpath + register, usecols=['user_id'])  # 打开训练集注册日志csv文件，只提取user_id列
    train_cre = pd.read_csv(trainpath + create, usecols=['user_id'])  # 打开训练集拍摄日志csv文件，只提取user_id列
    train_lau = pd.read_csv(trainpath + launch, usecols=['user_id'])  # 打开训练集启动日志csv文件，只提取user_id列
    train_act = pd.read_csv(trainpath + activity, usecols=['user_id'])  # 打开训练集行为日志csv文件，只提取user_id列
    train_data_id = np.unique(pd.concat([train_reg, train_cre, train_lau, train_act]))  # 先将四个训练集的表的id合并，然后去重

    test_reg = pd.read_csv(testpath + register, usecols=['user_id'])  # 打开测试集注册日志csv文件，只提取user_id列
    test_cre = pd.read_csv(testpath + create, usecols=['user_id'])  # 打开测试集拍摄日志csv文件，只提取user_id列
    test_lau = pd.read_csv(testpath + launch, usecols=['user_id'])  # 打开测试集启动日志csv文件，只提取user_id列
    test_act = pd.read_csv(testpath + activity, usecols=['user_id'])  # 打开测试集行为日志csv文件，只提取user_id列
    test_data_id = np.unique(pd.concat([test_reg, test_cre, test_lau, test_act]))  # 先将四个测试集的表的id合并，然后去重，对user_id排序

    train_label = []  # 训练属性
    for i in train_data_id:  # 如果训练集的user_id在测试集中出现，则标记为1，否则为0
        if i in test_data_id:
            train_label.append(1)
        else:
            train_label.append(0)
    train_data = pd.DataFrame()  # 创建空的数据帧
    train_data['user_id'] = train_data_id  # 训练数据帧的user_id列被赋为训练数据的用户id
    train_data['label'] = train_label  # 训练标签
    return train_data  # 返回训练数据帧


def get_test(testpath):
    test_reg = pd.read_csv(testpath + register, usecols=['user_id'])  # 提取user_id列
    test_cre = pd.read_csv(testpath + create, usecols=['user_id'])
    test_lau = pd.read_csv(testpath + launch, usecols=['user_id'])
    test_act = pd.read_csv(testpath + activity, usecols=['user_id'])
    test_data_id = np.unique(pd.concat([test_reg, test_cre, test_lau, test_act]))  # 用户id列合并去重排序
    test_data = pd.DataFrame()  # 创建空的数据帧
    test_data['user_id'] = test_data_id
    return test_data  # 返回测试数据帧


# 得到拍摄日志的特征
def get_create_feature(row):
    feature = pd.Series()  # 创建类似于一维数组的对象
    feature['user_id'] = list(row['user_id'])[0]
    return feature


def get_register_feature(row):
    feature = pd.Series()
    feature['user_id'] = list(row['user_id'])[0]
    return feature


def get_launch_feature(row):
    feature = pd.Series()
    feature['user_id'] = list(row['user_id'])[0]
    return feature


def get_activity_feature(row):
    feature = pd.Series()
    feature['user_id'] = list(row['user_id'])[0]
    return feature


# 处理特征
def deal_feature(path, user_id):
    reg = pd.read_csv(path + register)  # 读取csv文件
    cre = pd.read_csv(path + create)
    lau = pd.read_csv(path + launch)
    act = pd.read_csv(path + activity)
    feature = pd.DataFrame()  # 创建空的数据帧
    feature['user_id'] = user_id  # 测试集用户id

    # create 表
    #    创建了几个视频
    # 创建视频相减相隔天数均值、方差、最大值、最小值、峰度、偏度，最后一个
    # 记录的最后一次创建视频减去此时刻的最大一天
    # 一天最大弄了几次
    cre['max_day'] = np.max(reg['register_day'])  # 用户注册日期的最晚值，拍摄的最大值
    cre_feature = cre.groupby('user_id', sort=True).apply(get_create_feature)
    feature = pd.merge(feature, pd.DataFrame(cre_feature), on='user_id', how='left')  # 以user_id为键连接，以feature的id顺序为准
    print('create表特征提取完毕')

    # register 表
    # 注册类型
    # 设备类型
    reg['max_day'] = np.max(reg['register_day'])
    reg_feature = reg.groupby('user_id', sort=True).apply(get_register_feature)
    feature = pd.merge(feature, pd.DataFrame(reg_feature), on='user_id', how='left')
    print('register表特征提取完毕')

    # launch 表
    # 登录次数
    # 间隔均值、方差、最大值、最小值、峰度、偏度，最后一个
    # 最后一次打开app减去此时刻的最大一天
    lau['max_day'] = np.max(reg['register_day'])
    lau_feature = lau.groupby('user_id', sort=True).apply(get_launch_feature)
    feature = pd.merge(feature, pd.DataFrame(lau_feature), on='user_id', how='left')
    print('launch表特征提取完毕')

    # activity 表
    # 总体上 个数，求unique之后，均值、方差、最大值、最小值、峰度、偏度，最后一个
    
    # 统计page 0 1 2 3 4个数
    # 统计总page 0 1 2 3 4占总个数的频次
    # 统计action_type 0 1 2 3 4 5 个数
    # 统计action_type 0 1 2 3 4 5 占总个数的频次
    # 重复的video_id个数最大值
    # 重复的author_id个数最大值
    # 针对每一天的总个数 个数 均值、方差、最大值、最小值、峰度、偏度、最后一个
    # 针对每一天的总个数diff 个数 均值、方差、最大值、最小值、峰度、偏度、最后一个
    # 针对每一天page 0 1 2 3 4个数 序列  个数 均值、方差、最大值、最小值、峰度、偏度、最后一个
    # 针对每一天page 0 1 2 3 4占总个数的频次 序列 均值、方差、最大值、最小值、峰度、偏度、最后一个
    # 针对每一天action_type 0 1 2 3 4 5 个数 序列  个数 均值、方差、最大值、最小值、峰度、偏度、最后一个
    # 针对每一天action_type 0 1 2 3 4 5占总个数的频次 序列 均值、方差、最大值、最小值、峰度、偏度、最后一个
    # 最后一次activity减去此时刻的最大一天
    act['max_day'] = np.max(reg['register_day'])
    act_feature = act.groupby('user_id', sort=True).apply(get_activity_feature)
    feature = pd.merge(feature, pd.DataFrame(act_feature), on='user_id', how='left')
    print('activity表特征提取完毕')
    return feature


# 构建特征
def get_data_feature():
    one_train_data = get_train_label(one_dataSet_train_path, one_dataSet_test_path)  # 得到训练数据的属性
    one_feature = deal_feature(one_dataSet_train_path, one_train_data['user_id'])  # 
    one_feature['label'] = one_train_data['label']  # 特征标签
    print('第一组训练数据特征提取完毕')

    two_train_data = get_train_label(two_dataSet_train_path, two_dataSet_test_path)
    two_feature = deal_feature(two_dataSet_train_path, two_train_data['user_id'])
    two_feature['label'] = two_train_data['label']
    print('第二组训练数据特征提取完毕')

    train_feature = pd.concat([one_feature, two_feature])  # 合并第一组训练提取的特征和第二组训练提取的特征
    train_feature.to_csv(train_path, index=False) # 保存特征，不添加序列
    print('训练数据存储完毕')

    test_data = get_test(three_dataSet_train_path)  # 测试csv文件用户id，train_data是数据帧
    test_feature = deal_feature(three_dataSet_train_path, test_data['user_id'])
    test_feature.to_csv(test_path, index=False)  # 去序列保存
    print('测试数据存储完毕')


get_data_feature()
