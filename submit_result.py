# -*- coding:utf-8 -*-
import pandas as pd
import numpy as np
# 使用sklearn库
from sklearn import model_selection
import sklearn.metrics

lgb = pd.read_csv('result/lgb_result.csv')
print(lgb)
res = lgb[lgb['result'] >= 0.5]
print(len(res))
del res['result']
res.to_csv('result/handle_result.txt', index=False, header=False)
