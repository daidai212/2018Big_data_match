# -*- coding: utf-8 -*-

import pandas as pd

# 定义的全局变量
all_dataSet_path = 'handled_data/all_dataSet.csv'
one_dataSet_train_path = 'handled_data/one_dataSet_train_'
one_dataSet_test_path = 'handled_data/one_dataSet_test_'
two_dataSet_train_path = 'handled_data/two_dataSet_train_'
two_dataSet_test_path = 'handled_data/two_dataSet_test_'
three_dataSet_train_path = 'handled_data/three_dataSet_train_'

# 第一数据集 以1-16数据 预测17-23某用户是否活跃   16--》7
# 第二数据集 以8-23数据 预测24-30某用户是否活跃   16--》7

# 测试集
# 第三数据集 以15-30数据 预测31-37某用户是否活跃


# 读取文件
launch = pd.read_csv('handled_data/app_launch_log.csv')
activity = pd.read_csv('handled_data/user_activity_log.csv')
register = pd.read_csv('handled_data/user_register_log.csv')
video = pd.read_csv('handled_data/video_create_log.csv')


# 以时间换分数据集函数
def cut_data_as_time(data_set_path, new_data_set_path, begin_day, end_day):
    # 把day符合的留下
    temp_register = register[(register['register_day'] >= begin_day) & (register['register_day'] <= end_day)]
    temp_create = video[(video['create_day'] >= begin_day) & (video['create_day'] <= end_day)]
    temp_launch = launch[(launch['launch_day'] >= begin_day) & (launch['launch_day'] <= end_day)]
    temp_activity = activity[(activity['activity_day'] >= begin_day) & (activity['activity_day'] <= end_day)]

    # 保存划分后的csv文件,删除索引
    temp_register.to_csv(new_data_set_path + 'register.csv', index=False)
    temp_create.to_csv(new_data_set_path + 'create.csv', index=False)
    temp_launch.to_csv(new_data_set_path + 'launch.csv', index=False)
    temp_activity.to_csv(new_data_set_path + 'activity.csv', index=False)

    print('根据起始和结束时间将数据集取出完成，存入：', new_data_set_path)


# 生成数据集函数
def generate_data_set():
    print('开始划分数据集...')

    # 以1-16数据 预测17-23某用户是否活跃   16--》7
    begin_day = 1
    end_day = 16
    cut_data_as_time(all_dataSet_path, one_dataSet_train_path, begin_day, end_day)
    begin_day = 17
    end_day = 23
    cut_data_as_time(all_dataSet_path, one_dataSet_test_path, begin_day, end_day)
    print('第一数据集（包括数据集和结果集）划分完成...')

    # 以8-23数据 预测24-30某用户是否活跃   16--》7
    begin_day = 8
    end_day = 23
    cut_data_as_time(all_dataSet_path, two_dataSet_train_path, begin_day, end_day)
    begin_day = 24
    end_day = 30
    cut_data_as_time(all_dataSet_path, two_dataSet_test_path, begin_day, end_day)
    print('第二数据集（包括数据集和结果集）划分完成...')

    # 以15-30数据 预测31-37某用户是否活跃  16--》7
    begin_day = 15
    end_day = 30
    cut_data_as_time(all_dataSet_path, three_dataSet_train_path, begin_day, end_day)
    print('第三数据集（包括数据集和结果集）划分完成...')
    print('成功划分三个数据集...')


# # 创建文件（传入文件名创建相应路径下的文件并返回）
# def open_file(file_name):
#     path = 'handled/' + file_name + '.csv'
#     f = open(path, 'rw')
#     return f


# 调用执行生成数据集函数
generate_data_set()
