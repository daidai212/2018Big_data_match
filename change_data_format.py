# -*- coding: utf-8 -*-

# 加载pandas库
import pandas as pd

# 实现数据集转换。。。。原始数据集header都没有。。。并且按id排序方便自己查看
now_data = 'aline'

app_launch_log = pd.read_csv('app_launch_log.txt', sep='\t', header=None)  # 将app_launch_log.txt读取为Ò数据帧对象，加列索引，指定分隔符为\t
app_launch_log = app_launch_log.sort_index(by=[0, 1])  # 对数据帧排序，默认为从小到大，先按0排序后按1排序
app_launch_log = app_launch_log.rename({0: 'user_id', 1: 'launch_day'},
                                       axis=1)  # 修改数据帧的列名，0改为user_id,1改为launch_day,对行进行操作
app_launch_log.to_csv('handled_data/app_launch_log.csv', index=0)  # 保存数据帧到csv文件，不保索引

user_activity_log = pd.read_csv('user_activity_log.txt', sep='\t', header=None)
user_activity_log = user_activity_log.sort_index(by=[0, 1])
user_activity_log = user_activity_log.rename({0: 'user_id', 1: 'activity_day',
                                              2: 'page', 3: 'video_id',
                                              4: 'author_id', 5: 'action_type'}, axis=1)
user_activity_log.to_csv('handled_data/user_activity_log.csv', index=0)

user_register_log = pd.read_csv('user_register_log.txt', sep='\t', header=None)
user_register_log = user_register_log.sort_index(by=[0, 1])
user_register_log = user_register_log.rename({0: 'user_id', 1: 'register_day',
                                              2: 'register', 3: 'device type'}, axis=1)
user_register_log.to_csv('handled_data/user_register_log.csv', index=0)

video_create_log = pd.read_csv('video_create_log.txt', sep='\t', header=None)
video_create_log = video_create_log.sort_index(by=[0, 1])
video_create_log = video_create_log.rename({0: 'user_id', 1: 'create_day'}, axis=1)
video_create_log.to_csv('handled_data/video_create_log.csv', index=0)
